﻿using BPersianCalender;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rangarang
{
    public partial class MainForm : Form
    {
        DataContext dataContext = new DataContext();
        LoadClass loadClass = new LoadClass();
        public int numberFactor = 0;
        public bool isEditMain=false;
        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            //
            comboBoxCustomer.DataSource = loadClass.bindInComoKala(dataContext.PersonalModels.ToList());
            comboBoxCustomer.DisplayMember = "name";
            comboBoxCustomer.ValueMember = "id";
            //

            //
            int kala = int.Parse(comboBoxCustomer.GetItemText(comboBoxCustomer.SelectedValue));
            gridViewMain.DataSource = loadClass.bingInGridViewOrder(dataContext.orderModels
                .Where(c=>c.personalModel_id==kala).ToList());
            gridViewMain.Update();
            gridViewMain.Refresh();
            //        
        }

        private void createOrder_Click(object sender, EventArgs e)
        {
            isEditMain = false;
            OrderForm orderForm = new OrderForm(this);
            orderForm.ShowDialog();
            
        }

        private void comboBoxCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            

        }

        private void removeOrder_Click (object sender, EventArgs e)
        {
            if (gridViewMain.Rows.Count > 0)
            {

                foreach (DataGridViewRow r in gridViewMain.SelectedRows)
                {   
                    int j= (int)gridViewMain.SelectedRows[0].Cells[0].Value;
                    orderModel order=dataContext.orderModels.Where(a=>a.number==j).First();
                    dataContext.orderModels.Remove(order);
                    dataContext.SaveChanges();
                    
                    int kala = int.Parse(comboBoxCustomer.GetItemText(comboBoxCustomer.SelectedValue));
                    gridViewMain.DataSource = loadClass.bingInGridViewOrder(dataContext.orderModels
                        .Where(c => c.personalModel_id == kala).ToList());
                    gridViewMain.Update();
                    gridViewMain.Refresh();
                }
            }
        }

        private void searchOrder_Click(object sender, EventArgs e)
        {
            try
            {

                int kala = int.Parse(comboBoxCustomer.GetItemText(comboBoxCustomer.SelectedValue));
                DateTime firstDate = loadClass.getCalenderFromPersian(persianDateFirst.Text);
                DateTime lastTime = loadClass.getCalenderFromPersian(persianDateSecond.Text);
                //firstDatePicker.Value;
                //DateTime lastTime = secondDatePicker.Value;
                

                gridViewMain.DataSource = loadClass.bingInGridViewOrder(dataContext.orderModels
                    .Where(c => c.personalModel_id == kala)
                    .Where(c=>c.date>=firstDate)
                    .Where(c=>c.date<lastTime)
                    .ToList());
                gridViewMain.Update();
                gridViewMain.Refresh();
            }
            catch (Exception a)
            {
                Console.WriteLine(a.Message);
            }
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void editOrder_Click(object sender, EventArgs e)
        {
            //gridViewMain.SelectedRows[0].Cells
            isEditMain = true;
            try
            {
                numberFactor= (int)gridViewMain.SelectedRows[0].Cells[0].Value;
                OrderForm orderForm = new OrderForm(this);
                orderForm.ShowDialog();
            }catch(Exception aa)
            {
                
            }
            
        }
    }
}