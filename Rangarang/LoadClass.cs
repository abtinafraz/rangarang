﻿using MD.PersianDateTime;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rangarang
{
    class LoadClass
    {

        public DateTime getCalenderFromPersian (string persianDate)
        {
           string[] j= persianDate.Split('/');
            Calendar persian = new PersianCalendar();

            int year = int.Parse(j[0]);
            int month = int.Parse(j[1]);
            int day = int.Parse(j[2]);

            DateTime date = new DateTime(year, month, day, persian);
            return date;
        }
        public string miladiToPerisan(DateTime d)
        {
            //DateTime d = DateTime.Parse(GregorianDate);
            PersianCalendar pc = new PersianCalendar();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(pc.GetYear(d));
            stringBuilder.Append("/");
            stringBuilder.Append(pc.GetMonth(d));
            stringBuilder.Append("/");
            stringBuilder.Append(pc.GetDayOfMonth(d));


            //Console.WriteLine(string.Format("{0}/{1}/{2}", pc.GetYear(d), pc.GetMonth(d), pc.GetDayOfMonth(d)));

            return stringBuilder.ToString();
        }


        public List<string> getCellsMainGridview(DataGridView dataGridView)
        {
            List<string> s = new List<string>();
            s.Add(dataGridView.SelectedRows[0].Cells[0].ToString()); //FactorNumber
            s.Add(dataGridView.SelectedRows[0].Cells[1].ToString()); //Customer
            s.Add(dataGridView.SelectedRows[0].Cells[2].ToString()); //Date
            s.Add(dataGridView.SelectedRows[0].Cells[2].ToString()); //sum
            return s;
        }

        public BindingSource bindInComoKala(List<ProductModel> aa)
        {
            BindingSource a = new BindingSource();
            a.DataSource = aa;
            return a;
        }

        public int orderLastId(DataContext dataContext)
        {
           int count= dataContext.orderModels.Count();
            if(count==0)
                return 1;
            else
            {
               int a= dataContext.orderModels.OrderByDescending(u => u.id).FirstOrDefault().id;
                return a+1;
            }
        }

        public int factorLastId(DataContext dataContext)
        {
            int count = dataContext.orderModels.Count();
            if (count == 0)
                return 1;
            else
            {
                int a = dataContext.orderModels.OrderByDescending(u => u.number).FirstOrDefault().id;
                return a + 1;
            }
        }

        public BindingSource bindInComoKala(List<PersonalModel> aa)
        {
            BindingSource a = new BindingSource();
            a.DataSource = aa;
            return a;
        }

        public void onChangeCountOrPrice(TextBox priceVahed,TextBox count,TextBox jamKol)
        {
            int price = 0;
            int tedad = 0;
            if (int.TryParse(priceVahed.Text, out price))
            {
                if (int.TryParse(count.Text, out tedad))
                {
                    jamKol.Text = (tedad * price).ToString();

                }

            }
        }

        public void onChangeComoKalaInDetailForm(DataContext dbContext,ComboBox kalaComo,TextBox priceVahed)
        {

            int kala = int.Parse(kalaComo.GetItemText(kalaComo.SelectedValue));
            ProductModel p = dbContext.ProductModels.Where(c => c.id == kala).First();
            priceVahed.Text = (p.price).ToString();
        }

        public BindingSource bingInGridViewOrder(DbSet<orderDetailModel> orderDetails)
        {
            BindingSource bindingSource = new BindingSource();
            bindingSource.DataSource = (orderDetails).Select(c => new
            {
                nameProduct = c.ProductModel.name,
                codeProduct = c.ProductModel.code,
                price = c.price,
                count = c.count,
                sumprice = c.sumprice,
            });
            return bindingSource;
        }
        public BindingSource bingInGridViewOrder(List<orderModel> order)
        {
            BindingSource bindingSource = new BindingSource();
            bindingSource.DataSource = (order).Select(c => new
            {
                
                FactorNumber = c.number,
                Customer = c.PersonalModel.name,
                //date = c.date,
                date = miladiToPerisan(c.date),
                Sum = c.orderDetailModels.Sum(d=>d.sumprice),
            });
            return bindingSource;
        }
    }
}
