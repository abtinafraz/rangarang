﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rangarang
{
    public partial class DetailForm : Form
    {
        LoadClass load = new LoadClass();
        public OrderForm orderForm;


        public DetailForm()
        {
            InitializeComponent();
        }

        public DetailForm(OrderForm order)
        {
            InitializeComponent();
            orderForm = order as OrderForm;
        }

        private void DetailForm_Load(object sender, EventArgs e)
        {
            // load Product for create
            if (!orderForm.isEdit)
            {
                List<ProductModel> productModels = orderForm.dataContext.ProductModels.ToList();
                kalaComo.DisplayMember = "name";
                kalaComo.ValueMember = "id";
                kalaComo.DataSource = load.bindInComoKala(productModels);
            }
            // load Product for edit
            else
            {
                List<ProductModel> productModels = orderForm.dataContext.ProductModels.ToList();
                kalaComo.DisplayMember = "name";
                kalaComo.ValueMember = "id";
                kalaComo.DataSource = load.bindInComoKala(productModels);

                count.Text= orderForm.orderDetailEdit.count.ToString();
                priceVahed.Text = orderForm.orderDetailEdit.price.ToString();

               
                //orderForm.dataContext.orderDetailModels.Where(a => a.)
            }
            
            //





        }

        private void save_Click(object sender, EventArgs e)
        {

            //for create
            if (!orderForm.isEdit)
            {
                int kala = int.Parse(kalaComo.GetItemText(kalaComo.SelectedValue));
                int price = int.Parse(priceVahed.Text);
                int tedad = int.Parse(count.Text);
                int jam = int.Parse(jamKol.Text);



                orderDetailModel orderDetail = new orderDetailModel();
                orderDetail.product_id = kala;
                orderDetail.price = price;
                orderDetail.count = tedad;
                orderDetail.order_id = load.orderLastId(orderForm.dataContext);
                orderDetail.sumprice = jam;
                orderForm.dataContext.orderDetailModels.Local.Add(orderDetail);

                //
                BindingSource bindingSource = new BindingSource();
                bindingSource.DataSource = (orderForm.dataContext.orderDetailModels.Local.ToList()).Select(c => new
                {
                    nameProduct = c.ProductModel.name,
                    codeProduct = c.ProductModel.code,
                    price = c.price,
                    count = c.count,
                    sumprice = c.sumprice,
                });


                orderForm.tableGrid.DataSource = bindingSource;
                orderForm.Update();
                orderForm.Refresh();

                this.Close();

            }
            // for edit and update
            else
            {
                int kala = int.Parse(kalaComo.GetItemText(kalaComo.SelectedValue));
                int price = int.Parse(priceVahed.Text);
                int tedad = int.Parse(count.Text);
                int jam = int.Parse(jamKol.Text);

                var y = orderForm.dataContext
                    .orderDetailModels
                    .Local
                    .Where(a => a.ProductModel == orderForm.orderDetailEdit.ProductModel
                    &&
                    a.price == orderForm.orderDetailEdit.price
                    &&
                    a.count == orderForm.orderDetailEdit.count
                    ).First();

                y.count = tedad;
                y.product_id = kala;
                y.price = price;
                y.sumprice = jam;


                BindingSource bindingSource = new BindingSource();
                bindingSource.DataSource = (orderForm.dataContext.orderDetailModels.Local.ToList()).Select(c => new
                {
                    nameProduct = c.ProductModel.name,
                    codeProduct = c.ProductModel.code,
                    price = c.price,
                    count = c.count,
                    sumprice = c.sumprice,
                });


                orderForm.tableGrid.DataSource = bindingSource;
                orderForm.Update();
                orderForm.Refresh();

                this.Close();
            }
        }

        private void kalaComo_SelectedIndexChanged(object sender, EventArgs e)
        {
            load.onChangeComoKalaInDetailForm(orderForm.dataContext, kalaComo, priceVahed);
        }

        private void count_TextChanged(object sender, EventArgs e)
        {
            load.onChangeCountOrPrice(priceVahed, count, jamKol);

        }

        private void jamKol_TextChanged(object sender, EventArgs e)
        {
            load.onChangeCountOrPrice(priceVahed, count, jamKol);

        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
