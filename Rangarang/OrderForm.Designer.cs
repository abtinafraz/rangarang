﻿namespace Rangarang
{
    partial class OrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableGrid = new System.Windows.Forms.DataGridView();
            this.removeGrid = new System.Windows.Forms.Label();
            this.editGridview = new System.Windows.Forms.Label();
            this.createDatail = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxCustomer = new System.Windows.Forms.ComboBox();
            this.lala = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.sabtnahayi = new System.Windows.Forms.ToolStripStatusLabel();
            this.cancel = new System.Windows.Forms.ToolStripStatusLabel();
            this.persianDate = new BPersianCalender.BPersianCalenderTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.tableGrid)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableGrid
            // 
            this.tableGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableGrid.Location = new System.Drawing.Point(44, 142);
            this.tableGrid.Name = "tableGrid";
            this.tableGrid.Size = new System.Drawing.Size(734, 285);
            this.tableGrid.TabIndex = 26;
            // 
            // removeGrid
            // 
            this.removeGrid.AutoSize = true;
            this.removeGrid.Location = new System.Drawing.Point(640, 115);
            this.removeGrid.Name = "removeGrid";
            this.removeGrid.Size = new System.Drawing.Size(28, 13);
            this.removeGrid.TabIndex = 25;
            this.removeGrid.Text = "حذف";
            this.removeGrid.Click += new System.EventHandler(this.removeGrid_Click);
            // 
            // editGridview
            // 
            this.editGridview.AutoSize = true;
            this.editGridview.Location = new System.Drawing.Point(694, 115);
            this.editGridview.Name = "editGridview";
            this.editGridview.Size = new System.Drawing.Size(38, 13);
            this.editGridview.TabIndex = 24;
            this.editGridview.Text = "ویرایش";
            this.editGridview.Click += new System.EventHandler(this.editGridview_Click);
            // 
            // createDatail
            // 
            this.createDatail.AutoSize = true;
            this.createDatail.Location = new System.Drawing.Point(748, 115);
            this.createDatail.Name = "createDatail";
            this.createDatail.Size = new System.Drawing.Size(30, 13);
            this.createDatail.TabIndex = 23;
            this.createDatail.Text = "ایجاد";
            this.createDatail.Click += new System.EventHandler(this.createDatail_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(318, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "تاریخ";
            // 
            // comboBoxCustomer
            // 
            this.comboBoxCustomer.FormattingEnabled = true;
            this.comboBoxCustomer.Location = new System.Drawing.Point(623, 54);
            this.comboBoxCustomer.Name = "comboBoxCustomer";
            this.comboBoxCustomer.Size = new System.Drawing.Size(109, 21);
            this.comboBoxCustomer.TabIndex = 20;
            // 
            // lala
            // 
            this.lala.AutoSize = true;
            this.lala.Location = new System.Drawing.Point(748, 57);
            this.lala.Name = "lala";
            this.lala.Size = new System.Drawing.Size(40, 13);
            this.lala.TabIndex = 19;
            this.lala.Text = "مشتری";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Top;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sabtnahayi,
            this.cancel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.statusStrip1.Size = new System.Drawing.Size(790, 22);
            this.statusStrip1.TabIndex = 18;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // sabtnahayi
            // 
            this.sabtnahayi.Name = "sabtnahayi";
            this.sabtnahayi.Size = new System.Drawing.Size(56, 17);
            this.sabtnahayi.Text = "ثبت نهایی";
            this.sabtnahayi.Click += new System.EventHandler(this.sabtnahayi_Click);
            // 
            // cancel
            // 
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(44, 17);
            this.cancel.Text = "انصراف";
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // persianDate
            // 
            this.persianDate.Location = new System.Drawing.Point(158, 60);
            this.persianDate.Miladi = new System.DateTime(((long)(0)));
            this.persianDate.Name = "persianDate";
            this.persianDate.NowDateSelected = false;
            this.persianDate.ReadOnly = true;
            this.persianDate.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.persianDate.SelectedDate = null;
            this.persianDate.Shamsi = null;
            this.persianDate.Size = new System.Drawing.Size(142, 20);
            this.persianDate.TabIndex = 28;
            this.persianDate.TextChanged += new System.EventHandler(this.persianDate_TextChanged);
            // 
            // OrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 450);
            this.Controls.Add(this.persianDate);
            this.Controls.Add(this.tableGrid);
            this.Controls.Add(this.removeGrid);
            this.Controls.Add(this.editGridview);
            this.Controls.Add(this.createDatail);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxCustomer);
            this.Controls.Add(this.lala);
            this.Controls.Add(this.statusStrip1);
            this.Name = "OrderForm";
            this.Text = "OrderForm";
            this.Load += new System.EventHandler(this.OrderForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tableGrid)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView tableGrid;
        private System.Windows.Forms.Label removeGrid;
        private System.Windows.Forms.Label editGridview;
        private System.Windows.Forms.Label createDatail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxCustomer;
        public System.Windows.Forms.Label lala;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel sabtnahayi;
        private System.Windows.Forms.ToolStripStatusLabel cancel;
        private BPersianCalender.BPersianCalenderTextBox persianDate;
    }
}