﻿namespace Rangarang
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gridViewMain = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxCustomer = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.aaaaaa = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.createOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.editOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.removeOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.searchOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.exit = new System.Windows.Forms.ToolStripMenuItem();
            this.persianDateFirst = new BPersianCalender.BPersianCalenderTextBox();
            this.persianDateSecond = new BPersianCalender.BPersianCalenderTextBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMain)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gridViewMain);
            this.groupBox2.Location = new System.Drawing.Point(-15, 137);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(803, 313);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            // 
            // gridViewMain
            // 
            this.gridViewMain.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridViewMain.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.gridViewMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridViewMain.Location = new System.Drawing.Point(69, 19);
            this.gridViewMain.Name = "gridViewMain";
            this.gridViewMain.Size = new System.Drawing.Size(680, 263);
            this.gridViewMain.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.persianDateSecond);
            this.groupBox1.Controls.Add(this.persianDateFirst);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBoxCustomer);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.aaaaaa);
            this.groupBox1.Location = new System.Drawing.Point(-15, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(803, 87);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(254, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "تا تاریخ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(532, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "از تاریخ";
            // 
            // comboBoxCustomer
            // 
            this.comboBoxCustomer.FormattingEnabled = true;
            this.comboBoxCustomer.Location = new System.Drawing.Point(640, 60);
            this.comboBoxCustomer.Name = "comboBoxCustomer";
            this.comboBoxCustomer.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCustomer.TabIndex = 2;
            this.comboBoxCustomer.SelectedIndexChanged += new System.EventHandler(this.comboBoxCustomer_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(767, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "نام مشتری";
            // 
            // aaaaaa
            // 
            this.aaaaaa.AutoSize = true;
            this.aaaaaa.Location = new System.Drawing.Point(767, 16);
            this.aaaaaa.Name = "aaaaaa";
            this.aaaaaa.Size = new System.Drawing.Size(40, 13);
            this.aaaaaa.TabIndex = 0;
            this.aaaaaa.Text = "جستجو";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createOrder,
            this.editOrder,
            this.removeOrder,
            this.searchOrder,
            this.exit});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // createOrder
            // 
            this.createOrder.Name = "createOrder";
            this.createOrder.Size = new System.Drawing.Size(42, 20);
            this.createOrder.Text = "ایجاد";
            this.createOrder.Click += new System.EventHandler(this.createOrder_Click);
            // 
            // editOrder
            // 
            this.editOrder.Name = "editOrder";
            this.editOrder.Size = new System.Drawing.Size(55, 20);
            this.editOrder.Text = "ویرایش";
            this.editOrder.Click += new System.EventHandler(this.editOrder_Click);
            // 
            // removeOrder
            // 
            this.removeOrder.Name = "removeOrder";
            this.removeOrder.Size = new System.Drawing.Size(44, 20);
            this.removeOrder.Text = "حذف";
            this.removeOrder.Click += new System.EventHandler(this.removeOrder_Click);
            // 
            // searchOrder
            // 
            this.searchOrder.Name = "searchOrder";
            this.searchOrder.Size = new System.Drawing.Size(53, 20);
            this.searchOrder.Text = "جستجو";
            this.searchOrder.Click += new System.EventHandler(this.searchOrder_Click);
            // 
            // exit
            // 
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(44, 20);
            this.exit.Text = "خروج";
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // persianDateFirst
            // 
            this.persianDateFirst.Location = new System.Drawing.Point(374, 56);
            this.persianDateFirst.Miladi = new System.DateTime(((long)(0)));
            this.persianDateFirst.Name = "persianDateFirst";
            this.persianDateFirst.NowDateSelected = false;
            this.persianDateFirst.ReadOnly = true;
            this.persianDateFirst.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.persianDateFirst.SelectedDate = null;
            this.persianDateFirst.Shamsi = null;
            this.persianDateFirst.Size = new System.Drawing.Size(142, 20);
            this.persianDateFirst.TabIndex = 29;
            // 
            // persianDateSecond
            // 
            this.persianDateSecond.Location = new System.Drawing.Point(96, 56);
            this.persianDateSecond.Miladi = new System.DateTime(((long)(0)));
            this.persianDateSecond.Name = "persianDateSecond";
            this.persianDateSecond.NowDateSelected = false;
            this.persianDateSecond.ReadOnly = true;
            this.persianDateSecond.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.persianDateSecond.SelectedDate = null;
            this.persianDateSecond.Shamsi = null;
            this.persianDateSecond.Size = new System.Drawing.Size(142, 20);
            this.persianDateSecond.TabIndex = 30;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewMain)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxCustomer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label aaaaaa;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem createOrder;
        private System.Windows.Forms.ToolStripMenuItem editOrder;
        private System.Windows.Forms.ToolStripMenuItem removeOrder;
        private System.Windows.Forms.ToolStripMenuItem searchOrder;
        private System.Windows.Forms.ToolStripMenuItem exit;
        public System.Windows.Forms.DataGridView gridViewMain;
        private BPersianCalender.BPersianCalenderTextBox persianDateSecond;
        private BPersianCalender.BPersianCalenderTextBox persianDateFirst;
    }
}

