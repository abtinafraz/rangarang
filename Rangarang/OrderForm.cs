﻿using MD.PersianDateTime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rangarang
{
    public partial class OrderForm : Form
    {
        LoadClass load = new LoadClass();
        public DataContext dataContext = new DataContext();
        public MainForm main;
        public orderDetailModel orderDetailEdit=new orderDetailModel();
        public bool isEdit = false;
        public OrderForm()
        {
            InitializeComponent();
        }
        public OrderForm(MainForm mainForm)
        {
            InitializeComponent();
            this.main = mainForm as MainForm;
            
            
        }

        private void OrderForm_Load(object sender, EventArgs e)
        {
            

            // Load  create
            if (!main.isEditMain)
            {
                List<PersonalModel> productModels = dataContext.PersonalModels.ToList();
                comboBoxCustomer.DisplayMember = "name";
                comboBoxCustomer.ValueMember = "id";
                comboBoxCustomer.DataSource = load.bindInComoKala(productModels);

            }
            // Load Edit
            else
            {
                orderModel order = dataContext.orderModels
                    .Where(a => a.number == main.numberFactor)
                    .FirstOrDefault();
                List<PersonalModel> productModels = new List<PersonalModel>();
                productModels.Add(order.PersonalModel);
                comboBoxCustomer.DisplayMember = "name";
                comboBoxCustomer.ValueMember = "id";
                comboBoxCustomer.DataSource = load.bindInComoKala(productModels);
            
                
                List<orderDetailModel> orderDetailGet=order.orderDetailModels.ToList();
                BindingSource bindingSource = new BindingSource();
                bindingSource.DataSource = (orderDetailGet).Select(c => new
                {
                    nameProduct = c.ProductModel.name,
                    codeProduct = c.ProductModel.code,
                    price = c.price,
                    count = c.count,
                    sumprice = c.sumprice,
                });
                tableGrid.DataSource = bindingSource;
                


            }
             



        }

        

        private void createDatail_Click(object sender, EventArgs e)
        {
            isEdit = false;
            DetailForm detailForm = new DetailForm(this);
            detailForm.ShowDialog();
        }

        private void sabtnahayi_Click(object sender, EventArgs e)
        {
            if (!main.isEditMain)
            {
                int id_person = int.Parse(comboBoxCustomer.GetItemText(comboBoxCustomer.SelectedValue));
                orderModel orderModel = new orderModel();
                orderModel.id = load.orderLastId(dataContext);
                orderModel.date = load.getCalenderFromPersian(persianDate.Text);
                
                orderModel.orderDetailModels = dataContext.orderDetailModels.Local.ToList();
                orderModel.personalModel_id = id_person;
                orderModel.number = load.factorLastId(dataContext);

                dataContext.orderModels.Add(orderModel);
                dataContext.SaveChanges();

                int kala = int.Parse(comboBoxCustomer.GetItemText(comboBoxCustomer.SelectedValue));
                main.gridViewMain.DataSource = load.bingInGridViewOrder(dataContext.orderModels
                    .Where(c => c.personalModel_id == kala).ToList());
                main.gridViewMain.Update();
                main.gridViewMain.Refresh();
                this.Close();
                Console.WriteLine("CREATE");
            }
            // edit for sabt nahayi :)
            else
            {
                int id_person = int.Parse(comboBoxCustomer.GetItemText(comboBoxCustomer.SelectedValue));

                var u = dataContext.orderModels.Where(a=>a.number==main.numberFactor).First();
                u.date = load.getCalenderFromPersian(persianDate.Text);
                
                u.orderDetailModels = dataContext.orderDetailModels.Local.ToList();
                u.personalModel_id = id_person;
                dataContext.SaveChanges();

                int kala = int.Parse(comboBoxCustomer.GetItemText(comboBoxCustomer.SelectedValue));
                main.gridViewMain.DataSource = load.bingInGridViewOrder(dataContext.orderModels
                    .Where(c => c.personalModel_id == kala).ToList());
                main.gridViewMain.Update();
                main.gridViewMain.Refresh();
                Console.WriteLine("UPDATE");
                this.Close();
                
            
            }


        }

        private void cancel_Click(object sender, EventArgs e)
        {
            load.getCalenderFromPersian(persianDate.Text);
            this.Close();
        }

        private void removeGrid_Click(object sender, EventArgs e)
        {
            try
            {

                string i= (string)tableGrid.SelectedRows[0].Cells[0].Value;
                int product_code = (int)tableGrid.SelectedRows[0].Cells[1].Value;
                int price = (int)tableGrid.SelectedRows[0].Cells[2].Value;
                int count = (int)tableGrid.SelectedRows[0].Cells[3].Value;

                Console.WriteLine(product_code+" "+price+" "+count);

                orderDetailModel oo = dataContext.orderDetailModels
                    .Local
                    .Where(aa=>aa.price==price && aa.ProductModel.code==product_code && aa.count==count)
                    .First();
                dataContext.orderDetailModels.Local.Remove(oo);

                BindingSource bindingSource = new BindingSource();


                bindingSource.DataSource = (dataContext.orderDetailModels.Local.ToList()).Select(c => new
                {
                    nameProduct = c.ProductModel.name,
                    codeProduct = c.ProductModel.code,
                    price = c.price,
                    count = c.count,
                    sumprice = c.sumprice,
                });


                tableGrid.DataSource = bindingSource;
                tableGrid.Update();
                tableGrid.Refresh();

            }
            catch(Exception yy)
            {
                Console.WriteLine(" ASD ");
            }

        }

        private void editGridview_Click(object sender, EventArgs e)
        {
            isEdit = true;
            try
            {
                int product_code = (int)tableGrid.SelectedRows[0].Cells[1].Value;
                int price = (int)tableGrid.SelectedRows[0].Cells[2].Value;
                int count = (int)tableGrid.SelectedRows[0].Cells[3].Value;
                orderDetailEdit.count = count;
                orderDetailEdit.price = price;
                orderDetailEdit.ProductModel = dataContext.ProductModels.Where(a => a.code == product_code).First();
                DetailForm detailForm = new DetailForm(this);
                detailForm.ShowDialog();
            }catch(Exception ww)
            {
                Console.WriteLine(ww.Message);
            }
            
        }

        private void persianDate_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
