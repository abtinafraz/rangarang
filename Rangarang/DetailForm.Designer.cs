﻿namespace Rangarang
{
    partial class DetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancel = new System.Windows.Forms.Label();
            this.save = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.jamKol = new System.Windows.Forms.TextBox();
            this.count = new System.Windows.Forms.TextBox();
            this.priceVahed = new System.Windows.Forms.TextBox();
            this.kalaComo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cancel
            // 
            this.cancel.AutoSize = true;
            this.cancel.Location = new System.Drawing.Point(193, 17);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(40, 13);
            this.cancel.TabIndex = 32;
            this.cancel.Text = "انصراف";
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // save
            // 
            this.save.AutoSize = true;
            this.save.Location = new System.Drawing.Point(239, 17);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(34, 13);
            this.save.TabIndex = 31;
            this.save.Text = "ذخیره";
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "ریال";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(182, 224);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "جمع کل";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(182, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "تعداد";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(182, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "قیمت واحد";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(182, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "کالا";
            // 
            // jamKol
            // 
            this.jamKol.Enabled = false;
            this.jamKol.Location = new System.Drawing.Point(36, 221);
            this.jamKol.Name = "jamKol";
            this.jamKol.Size = new System.Drawing.Size(121, 20);
            this.jamKol.TabIndex = 25;
            this.jamKol.TextChanged += new System.EventHandler(this.jamKol_TextChanged);
            // 
            // count
            // 
            this.count.Location = new System.Drawing.Point(36, 172);
            this.count.Name = "count";
            this.count.Size = new System.Drawing.Size(121, 20);
            this.count.TabIndex = 24;
            this.count.TextChanged += new System.EventHandler(this.count_TextChanged);
            // 
            // priceVahed
            // 
            this.priceVahed.Location = new System.Drawing.Point(36, 122);
            this.priceVahed.MaxLength = 300;
            this.priceVahed.Name = "priceVahed";
            this.priceVahed.Size = new System.Drawing.Size(121, 20);
            this.priceVahed.TabIndex = 23;
            // 
            // kalaComo
            // 
            this.kalaComo.FormattingEnabled = true;
            this.kalaComo.Location = new System.Drawing.Point(36, 76);
            this.kalaComo.Name = "kalaComo";
            this.kalaComo.Size = new System.Drawing.Size(121, 21);
            this.kalaComo.TabIndex = 22;
            this.kalaComo.SelectedIndexChanged += new System.EventHandler(this.kalaComo_SelectedIndexChanged);
            // 
            // DetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 278);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.save);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.jamKol);
            this.Controls.Add(this.count);
            this.Controls.Add(this.priceVahed);
            this.Controls.Add(this.kalaComo);
            this.Name = "DetailForm";
            this.Text = "DetailForm";
            this.Load += new System.EventHandler(this.DetailForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label cancel;
        private System.Windows.Forms.Label save;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox jamKol;
        private System.Windows.Forms.TextBox count;
        private System.Windows.Forms.TextBox priceVahed;
        private System.Windows.Forms.ComboBox kalaComo;
    }
}